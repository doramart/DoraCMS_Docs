/** @format */

module.exports = {
  title: 'DoraCMS中文文档',
  description: 'DoraCMS中文文档',
  base:'/doracms_docs/',
  smoothScroll: true,
  head: [['link', { rel: 'icon', href: '/images/favicon.ico' }]],
  themeConfig: {
    logo: '/images/logo.png',
    lastUpdated: '最后更新时间', //更新时间戳
    nav: [
      { text: '指南', link: '/guide/' },
      { text: '模板配置', link: '/template/' },
      { text: '插件', link: '/plugin/' },
      { text: '生态', link: '/market/' },
      {
        text: '关于',
        items: [
          { text: '联系我', link: '/about/contact_us' },
          { text: '赞助我', link: '/about/suport_us' },
        ],
      },
      {
        text: 'ApiDoc',
        link: 'https://www.html-js.cn/static/apidoc/index.html',
      },
      { text: 'GitHub', link: 'https://github.com/doramart/DoraCMS' },
    ],
    sidebar: {
      '/guide/': [
        {
          title: '指南',
          collapsable: false,
          sidebarDepth: 2,
          children: [
            '/guide/',
            '/guide/dev',
            '/guide/docker',
            '/guide/preparationEnv',
            '/guide/prod',
            '/guide/backstage',
          ],
        },
        {
          title: '其他',
          collapsable: false,
          sidebarDepth: 2,
          children: ['/other/faq', '/other/update'],
        },
      ],
      '/template/': [
        {
          title: '模板设置&开发',
          collapsable: false,
          sidebarDepth: 2,
          children: ['/template/', '/template/directoryStructure', '/template/template', '/template/showdata'],
        },
      ],
      '/plugin/': [
        {
          title: '插件开发',
          collapsable: false,
          sidebarDepth: 2,
          children: ['/plugin/'],
        },
      ],
    },
  },
};
