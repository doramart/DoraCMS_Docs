# 插件发布

我们都知道，`DoraCMS` 的插件放在 `lib/plugins` 路径下，根据 `eggjs` 的相关约定，插件稳定后，我们可以把插件发布到 `https://www.npmjs.com` 上，这样，我们在发布生产的时候，可以直接从网上下载，不用发布源码到服务器上了，[DoraCMS官网](https://www.html-js.cn) 目前就是这么做的，我们可以按下面的步骤操作：

> 该文档描述的是 DoraCMS 插件发布的流程，在实际操作中，你所发布的插件名称一定不要跟 DoraCMS 的插件重复，否则是无法正常发布到 npm 上的，例如，你可以把你的某个插件命名为 egg-dora1-ads ，但是一定不要是 egg-dora-ads ，因为 egg-dora-ads 在 npmjs 上已经存在了，非常重要！！！

# 准备工作

1.在代码根目录同级目录下，手动创建文件夹 `egg-cms-plugins`

<img src="https://i.loli.net/2020/02/04/Y9SwPz8ETaiglJ2.png" alt="WX20200204-194431@2x.png" style="zoom:50%;" />

 

2.打开 `publish.js` 修改 `basePluginForderPath` 路径为刚才创建的 `egg-cms-plugins` 文件夹路径

> lib -> plugin -> publish.js

<img src="https://i.loli.net/2020/02/04/xrq2jaAwokLTSXl.png" alt="WX20200204-195019@2x.png" style="zoom: 33%;" />

3.注册 `github` 账户 和 `npm` 账户

```javascript
// github 注册地址
https://github.com/join?source=login

// npm 注册地址
https://www.npmjs.com/signup
```

4.在 `github` 上依次创建自己的插件仓库 （目前免费的只支持公有仓库）。

> 一定注意，你需要花十分钟时间，依照 lib -> plugin 中的文件夹名称依次创建代码仓库。
<img src="https://i.loli.net/2020/02/04/FYUegfCjspWibwo.png" alt="WX20200204-195810@2x.png" style="zoom: 33%;" />

# 插件发布

1.修改 `publish.js`  ,设置需要发布的插件 `designatedPlugin`

> 路径 lib -> plugin -> publish.js ，不填默认发布所有插件

<img :src="$withBase('/images/publishPlugin/image-20200204201517708.png')" alt="image-20200204201517708" style="zoom:33%;" />

仍然在 `publish.js` ,将 `targetGitUrl` 的值红框部分修改成你的 `github`仓库地址
<img :src="$withBase('/images/publishPlugin/image-20200204202400982.png')" alt="image-20200204202400982">

2.配置 `npm`，添加 `npm` 用户，打开终端执行

```
npm adduser
```

<img :src="$withBase('/images/publishPlugin/image-20200204201933806.png')" alt="image-20200204201933806" style="zoom:33%;" />

根据提示输入刚才在 `npm` 上注册的用户信息

3.进入 `lib -> plugin` 目录，执行脚本

```
node publish.js
```

4.发布完成后，你可以在NPM上看到自己发布的插件

<img :src="$withBase('/images/publishPlugin/image-20200204215655116.png')" alt="image-20200204215655116" style="zoom:33%;" />