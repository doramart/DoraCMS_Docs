# 模板开发

>使用模板标签前请先通读 [Nunjucks](https://nunjucks.bootcss.com/) 文档

### 模版标签使用

在 `DoraCMS` 中我们把 `{% %}` 作为开始结束标签。`DoraCMS` 模版标签的是以下面的方式进行声明

  
```javascript
{% remote key="tabContents",api="content/getList",query='{"model":"1","pageSize":7,"isPaging":"0","typeId":"Ek7skiaw"}' %}
```

模版标签必须以 `{％` 开头，并以 `％}` 结尾的代码片段，如果不正确会引起程序致命性的错误，导致程序无法继续运行。

### 模版标签分析

标签中 `{％` 后的 `remote` 是固定的，通过 `remote` 标记，我们可以使用服务端定义的方法来请求数据

```javascript
key="tabContents"
```
这是开发者指定的变量，代表的是返回数组接收的变量。

```javascript
api="content/getList"
```
这是获取数据指定的api，在模板中展示的数据主要来自api，数据获取是异步的。

```javascript
query='{"model":"1","pageSize":7,"isPaging":"0","typeId":"Ek7skiaw"}'
```
`query` 是选填的，包含了在接口请求过程中需要携带的参数，`query` 中约定了几个参数可以传递：

|  变量名   | 默认值  | 备注  |
|  ----  | ----  | ----  |
| current  | 1 | 当前页码 | 
| pageSize  | 10 | 每页数量 | 
| isPaging  | 1 | 是否分页 0：不分页 1：分页 | 


这个形式所代表的是参数。每一个模型都为其模版标签定义了调用的参数。其中有一些调用参数是系统保留的参数，其对所有的模版标签都是有效的。

参数必须使用
```javascript
参数名="参数值"
```

的方式填写，多个参数之间使用’,’分开（如下例），参数值可使用双引号来包括，无论是什么形式的引号都必须是成对出现的。

```javascript
{% remote key="demoKey", 参数名="参数值",参数名="参数值",参数名="参数值" %}
```
请查看模版表情标签保留参数相关章节，以了解具体保留参数及其用途。

### 如何显示模版标签中的数据

默认情况下模版标签中的数据都是数组方式返回的，你都可以通过 你在模版标签中定义key的参数来接收返回的数组。
比如上例定义key为 'demoKey' ，你可以使用如下的方式来显示值：

```javascript
<ul>
{% for val in demoKey%}
<li><a href="{{val.name|get_url(val.id)}}">{{val.title}}</a></li>
{% endfor %}
</ul>
```




### 模版标签保留参数
下表为模版标签保留参数表，几乎所有的模版标签都支持这些保留参数设置

|  变量名   | 默认值  | 备注  |
|  ----  | ----  | ----  |
| key  | - | 定义接收数据参数 | 
| api  | - | 获取数据的接口 | 
| query  | - | 接口请求携带的参数 | 


下例中是首页请求热门标签的例子。

```javascript
{% remote key='hotTags',api="contentTag/getList",query='{"isPaging":"0"}' %}
{% for tagItem in hotTags %}
<a href="/tag/{{tagItem.name}}" target="_blank">{{tagItem.name}}</a>
{% endfor %}
```
