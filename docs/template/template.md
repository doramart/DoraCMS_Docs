# 模板安装


### 模板切换

前台页面展示基于 `egg-view-nunjucks` 进行模板渲染，基于此，DoraCMS 是可以进行整体风格切换的，您可以根据以下路径找到风格切换入口

```javascript
后台管理 -> 模板配置 -> 可用模板 -> 启用
```

>在后台的模板配置模块中，模板市场选项卡有模板可供下载。


### 模板创建

可以用通过下面的路径进入模板导入功能
```javascript
后台管理 -> 模板配置 -> 模板导入 -> 下载示例模板
```

您可以这么做
1. 在此下载示例模板

2. 通过修改 `tempconfig.json` 文件来介绍您的新模板信息

   > version 字段需要和当前使用的 DoraCMS 版本号一致
```javascript
[
    {
        "name": "dora简约",
        "alias": "dorawhite",
        "version": "v2.1.4",
        "sImg": "/images/demo.jpg",
        "author": "doramart",
        "comment": "DoraCMS默认模板，用最基础的白色展现页面，希望大家喜欢"
    }
]
```
3. 重新压缩模板文件夹为zip格式，并重命名，压缩文件名称和 `tempconfig.json` 中的 `alias` 对应的值一定要一致

4. 上传新的模板压缩包
5. 模板开发