
# 目录结构

#### 后台页面目录结构
>DoraCMS 后台页面是彼此独立的，每个页面相当于一个小工程，下面以管理员模块介绍其目录结构 

```
adminUser
├── README.md
├── babel.config.js
├── dist  （编译后目录）
│   ├── favicon.ico
│   ├── index.html
│   └── js
├── package.json
├── postcss.config.js
├── public
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── App.vue  (入口文件)
│   ├── api  (接口调用)
│   ├── assets  (静态资源)
│   ├── components  (公共租界)
│   ├── filters  (过滤器)
│   ├── icons  (svg小图标)
│   ├── lang  (国际化)
│   ├── main.js (入口文件)
│   ├── router  (路由)
│   ├── set-public-path.js (single-spa配置)
│   ├── store  (redux)
│   ├── styles (样式)
│   ├── utils  (公共方法)
│   └── views  (页面文件)
└── vue.config.js


```


#### 公共资源
> 在 publichMethod 目录下（backstage -> publicMethod），有必要介绍下各个文件的功能路径，这里包含了所有组件都需要引用的一些公共方法，保证每个组件编译后，相关的方法的统一。

```
publicMethods
├── auth.js (cookie操作)
├── axiosLoading.js  (后台数据加载的loading效果)
├── baseLang (基础国际化字典)
├── events.js  (页面初始化逻辑处理)
├── get-page-title.js (定义工程名称)
├── request.js  (封装接口请求方法)
├── sass  (公共的样式)
├── settings.js  (统一配置文件)
└── svg  (基础icon)
```