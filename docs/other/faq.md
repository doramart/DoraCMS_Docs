# FAQ(常见问题)

### 怎么取消后台登录的验证码？

* 登录后台，系统管理 -> 系统配置 -> 显示验证码 -> 保存
<img src="https://i.loli.net/2019/12/21/rwVEmQbqolcAkiU.png" alt="20191221153026.png" style="zoom:50%;" />

### 图片上传路径如何去掉前面的ip或域名（针对本地上传）

* 删除代码根目录下 `node_modules`,重新安装即可

### 部署到生产环境，服务启动正常，但是外网无法访问？

*  找到 `config.default.js` 修改配置

```
cluster: {
    listen: {
    port: 8080,
    hostname: '0.0.0.0',
    }
},
```

### 编译后台文件提示 `export` 部署内部或外部命令，也不是可运行的程序

* 该问题只在windows上会有，更新 github 上最新代码后重启即可

### 生产环境，项目启动后，前台访问报 500 错误

> 报错的原因很多，下面的步骤可以帮助你排查问题

* 请确认您已设置环境变量 NODE_ENV=production
* 确认 `mongodb` 连接正常
* 确认初始化数据已成功导入
* 确认 `config.prod.js` 中的ip 或域名配置正确

```
 // 服务地址配置
server_path: 'https://www.yourdomain.cn',
server_api: 'https://www.yourdomain/api',
```

* 项目启动时，在终端可以看到启动日志，您可以通过查看日志看到具体报错信息

### 修改 `js` 或 `css`, 页面没有反应

* 请强制刷新，或者修改缓存时间
![20191221155017.png](https://i.loli.net/2019/12/21/bi5B8xNmO7DroV9.png)

### 修改了后台的部分页面，生产上没有改变，静态文件链接到了 https://cdn.html-js.cn

* 后台页面和相关路由是以插件形式存在的，所以 `backstag` 中的后台页面和 `lib/plugin` 中的插件是有一定关联的，出现这种情况的原因是，你改了前台代码，但是与之关联的服务端是我的链接，你需要把下面的链接改成你自己的的，eg. `http://www.yourdomain.com/static/adminUser/js/app.js`
![QvE0sA.md.png](https://s2.ax1x.com/2019/12/21/QvE0sA.md.png)

### 开发环境下，如何修改js，css 等文件 使其生效？

由于一般情况下，生产上的`css` 和 `js` 等静态文件是需要压缩的，`DoraCMS` 在开发环境下，使用 `gulp` 监听文件改动，自动编译到对应目录，假设你当前使用的是系统默认模板 `dorawhite` 你可以这么做：

> 注意，最新版本nodejs跟gulp存在兼容性问题，目前在 nodejs v12.13.0 上是正常的

* 进入 `app/assets` 目录下，安装依赖

  ```javascript
  npm install
  ```

* 修改 `app/assets/gulp.js`

  > `app/assets` 目录下没有 `qingyulan` 文件夹的，可以加作者微信索要，因为是付费模板（并没有同时开源）

  ```javascript
  var tempforder = "dorawhite”; // 如果你用的是青于蓝模板，改成 qingyulan
  ```

* 依然在 `app/assets` 目录下，终端执行 `gulp`
<img :src="$withBase('/images/image-20200214104735475.png')" alt="image-20200214104735475">

* 修改 `app/assets` 目录下对应模板的静态资源后保存，`gulp` 会通过自动监听的方式编译到对应的模板文件夹中