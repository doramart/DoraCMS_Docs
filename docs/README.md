---
home: true
heroImage: /images/1056082359805415424.jpg
heroText: DoraCMS
tagline: 
actionText: 阅读文档
actionLink: /guide/
features:
- title: 普通建站者
  details: 您可以使用DoraCMS发布版进行网站快速搭建上线，选择您合适的皮肤切换风格，满足业务需求。
- title: 开发者
  details: 您可以使用DoraCMS快速进行二次开发，上手容易，需要基本的前端知识就可以建站，DoraCMS遵循MIT协议完全开源，所以代码可以免费商用。
- title: 拓展性
  details: DoraCMS是基于 nodejs 的内容管理系统，所用技术栈包含eggjs、mongodb、vue、single-spa 等。代码结构清晰，目录规划合理，项目整体考虑到了从普通用户使用，开发者开发，编译，发布的整个流程。
footer: MIT Licensed | 本文档由VuePress驱动
---

