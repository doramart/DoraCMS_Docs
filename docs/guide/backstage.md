# 后台页面开发

#### 后台页面热部署开发

1.修改 config.local.js 文件，假设我们要开发 `管理员模块` (adminUser)
>文件路径 root -> config -> config.local.js

将 dev_modules 中的 adminUser 注释打开：
```javascript
dev_modules: [
            // 'navbar',
            // 'dashboard',
            // 'adminGroup',
            'adminUser',
            // 'adminResource',
            // 'systemConfig',
            // 'backUpData',
            // 'systemOptionLog',
            // 'announce',
            // 'systemNotify',
            // 'ads',
            // 'contentTemp',
            // 'templateConfig',
            // 'versionManage',
            // 'content',
            // 'contentTags',
            // 'contentCategory',
            // 'contentMessage',
            // 'regUser',
            // 'helpCenter',
            // 'renderCms',
            // 'cmsTemplate',
            // 'plugin',
            // 'uploadFile'
        ],
```

2.进入 adminUser 目录下，运行工程
> root -> backstage -> adminUser

```javascript
npm run serve
```

3.登录进入后台，刷新页面
至此，你在 adminUser 目录下的修改会以热部署形式更新到页面上

4.编译 `adminUser` 模块  
修改文件 `buildModules.js`
> root -> backstage -> build

```javascript
// 指定打包模块，不填默认打包所有模块
let designatedModule = ['adminUser'];
```
在 `build` 目录下执行
```javascript
npm run buildDevModules
```


#### 发布到七牛云存储
由于后台管理的模块使用了微服务模式，我们可以把所有模块编译完成后上传到七牛云存储上，在项目发布到生产后，后台引用云上的资源更加方便，操作如下：

1.修改七牛配置
> root -> backstage -> build -> utils.js

找到七牛的 `accessKey`, `secretKey` 等信息，改成自己的

2.回到`build`目录下
> root -> backstage -> build

```javascript
npm run buildPrdModules
```

3.修改 config.default.js, 找到 `origin` 字段，修改为你的七牛cdn域名
>文件路径 root -> config -> config.default.js

```javascript
origin: 'https://cdn.yourcdn.cn',
```