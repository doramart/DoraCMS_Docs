# 开发环境

### Windows 下开发环境搭建

[Windows 下开发环境搭建](https://www.bilibili.com/video/av77251989/)  


### MAC OS 开发环境搭建

1.安装最新稳定版 NodeJS (12.13.0):
```javascript
https://nodejs.org/zh-cn/
```


2.设置环境变量（以mac为例，修改 .bash_profile文件）

```javascript
vi ~/.bash_profile

export NODE_ENV=development
MONGODBPATH=/Users/Dora/Documents/dora/soft/mongodb/bin
PATH="${MONGODBPATH}:${PATH}"
export PATH

source ~/.bash_profile
```

> 以上步骤做了两件事情：  
1.设置nodejs环境变量为 development,生产环境记得改为 production  
2.将mongodb bin 目录添加到全局变量中，便于在终端的任何位置执行mongo脚本,注意改成自己安装mongodb的实际路径  



3.安装并启动 Mongodb (++mongodb不要设置密码访问++)
```javascript
https://www.mongodb.com/download-center#community
```

4.安装全局依赖
```javascript
npm install egg-scripts -g   // eggjs 脚本执行
npm install gulp -g  // 静态资源构建
npm install apidoc -g  // api文档生成
```


5.安装本系统依赖（代码根目录）
```javascript
npm install
```

6.安装插件依赖
```
npm install mammoth node-schedule --save
```

7.初始化数据
```javascript
npm run init
```

> 网站图片资源可从这里获取
下载链接: https://pan.baidu.com/s/1th7Qlz4eJGNN3w_Tacl9AQ 提取码: jczt  ，解压后放到项目root下 app/public/upload (替换)。


8.开发模式启动
```javascript
npm run dev
```