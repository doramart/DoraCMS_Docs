# 生产环境部署

### 生产环境准备
>我们一般情况下使用linux作为服务器，以 `ubuntu server 18.04` 64位 为例

#### 安装nodejs。

1.推荐先做系统更新

```bash
apt-get update
```

2.Node.js 仅需要你的服务器上有一些基本系统和软件功能，比如 `make`、`gcc` 和 `wget` 之类的。如果你还没有安装它们，运行如下命令安装：

```bash
apt-get install python gcc make g++ wget
```

3.创建文件夹，在服务器上找个合适位置创建两个文件夹 soft, webapp。
```bash
mkdir soft
mkdir webapp
```

4.访问链接 Node JS Download Page 下载源代码.复制下面源代码的链接，进入刚才创建的soft目录下然后用 `wget` 下载，命令如下：

```bash
wget https://nodejs.org/dist/v12.13.0/node-v12.13.0.tar.gz
```




下载完成后解压:
```bash
tar -zxvf node-v12.13.0.tar.gz
```

5.安装 Node JS v12.13.0

现在可以开始使用下载好的源代码编译 Node.js。在开始编译前，你需要在 ubuntu server 上切换到源代码解压缩后的目录，运行 configure 脚本来配置源代码。  
 
```bash
./configure
```

现在运行命令 'make install' 编译安装 Node.js：
 
```bash
make install
```

make 命令会花费十几分钟完成编译，安静的等待一会................. 漫长的等待过后（大概10分钟），编译结束，我们验证一下是否安装成功：  
 
```bash
node -v
12.13.0
```

#### 安装mongodb

1.去 [mongod 官网](https://www.mongodb.com/download-center)  找最新版的链接，切换到soft目录下执行

```bash
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1804-4.2.1.tgz
```

2.解压缩到 /usr/local/mongodb/ 目录下 (需要提前在 local 目录下创建 mongodb 文件夹)

```bash
tar zxvf mongodb-linux-x86_64-ubuntu1804-4.2.1.tgz
mv mongodb-linux-x86_64-ubuntu1804-4.2.1 /usr/local/mongodb
```

3.在/usr/local/mongodb/目录下建立文件夹data、log

```bash
mkdir data
mkdir log
```
  
4.在/usr/local/mongodb/目录下创建配置文件mongodb.conf

```bash
dbpath=/usr/local/mongodb/data/  #数据存放路径
logpath=/usr/local/mongodb/log/mongodb.log #日志存放路径
#auth=true
fork=true
```

5.启动mongodb（注意此时不需要auth启动）

```bash
./mongod --config /usr/local/mongodb/mongodb.conf
```


6.进入mongodb的bin目录下执行数据导入：
>你需要提前把之前在本地调试好的数据备份，上传到服务器的指定目录，如 /home/yourusername/Documents/soft/data

```bash
cd /usr/local/mongodb/mongodb-linux-x86_64-ubuntu1804-4.2.1/bin
mongorestore -h 127.0.0.1:27017 -d doracms2 --drop /home/yourusername/Documents/soft/data
```

导入成功后是这样的：
![数据导入](https://cdn.html-js.cn/%E6%95%B0%E6%8D%AE%E5%AF%BC%E5%85%A5%E6%88%90%E5%8A%9F.png)


7.创建数据库管理员

还是在mongodb bin 目录,下执行下面的语句:
```bash
./mongo
...
use admin
```

添加管理员
```bash
db.createUser({user: "doracms",pwd: "doracms",roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]})
```
![添加admin用户](https://cdn.html-js.cn/%E8%AE%BE%E7%BD%AE%E6%95%B0%E6%8D%AE%E5%BA%93%E7%AE%A1%E7%90%86%E5%91%98%E5%AF%86%E7%A0%81.png)
添加doracms数据库用户
```bash
use doracms2
db.createUser( { user: "doramart", pwd: "doramart", roles: [ { role: "readWrite", db: "doracms2" }, ] } )
```
![添加doracms用户](https://cdn.html-js.cn/%E8%AE%BE%E7%BD%AEdoracms2%E5%AF%86%E7%A0%81.png)

添加完成后，我们可以通过 db.auth 的方式校验是否添加正确。

8.修改mongdb.conf , 将 auth=true 注释打开，把之前启动的mongdb挂掉，使用进程守护的方式重新启动数据库。
```bash
dbpath=/usr/local/mongodb/data/  #数据存放路径
logpath=/usr/local/mongodb/log/mongodb.log #日志存放路径
auth=true
fork=true
```
![修改启动数据库的参数](https://cdn.html-js.cn/%E4%BF%AE%E6%94%B9%E4%B8%BA%E9%89%B4%E6%9D%83%E6%A8%A1%E5%BC%8F.png)

启动mongodb：
```bash
./mongod --config /usr/local/mongodb/mongodb.conf 

```

#### 安装git
```bash
apt-get install git
```

#### 配置环境变量

1.修改环境变量配置文件 `profile`
```bash
vim /etc/profile
```
![环境变量](https://cdn.html-js.cn/%E8%AE%BE%E7%BD%AE%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F.png)
在文档最后面加入，保存并退出
```bash
export NODE_ENV=production
```

验证配置是否生效

![变量生效](https://cdn.html-js.cn/%E6%9F%A5%E7%9C%8B%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%E7%94%9F%E6%95%88.png)


### 部署代码

#### 修改配置

打开生产环境配置文件 config.prod.js
> root -> config -> config.prod.js

1.修改 mongodb 链接
>里面的数据跟你刚才配置的mongodb数据库用户名密码一致

```javascript
mongoose: {
    client: {
        url: 'mongodb://doramart:doramart@127.0.0.1:27017/doracms2',
        ...
    },
},
```

2.修改日志文件存储路径
```javascript
logger: {
    dir: '/home/doraData/logsdir/doracms',
},
```

3.修改静态资源路径
>路径可以不在代码文件夹，可以指定在服务器的某个目录，便于后面使用构建工具全量部署

```javascript
static: {
    prefix: '/static',
    dir: [path.join(appInfo.baseDir, 'app/public'), '/home/doraData/uploadFiles/static'],
    maxAge: 31536000,
},
```


4.修改服务器地址和api地址
>假设你的域名和服务器ip已经完成dns解析

```javascript
server_path: 'https://www.yourdomain.cn',
server_api: 'https://www.yourdomain.cn/api',
```


#### 准备代码

1.为了方便起见，你可以在 [码云](https://www.gitee.com) 上建立一个私有的空间存放自己的代码，把本地代码上传上去

2.通过ssh方式进入服务器中刚刚创建的 `webapp` 目录下，执行：

>下面的地址是你建立好仓库后的代码库地址

`git clone https://gitee.com/yourname/DoraCMS`

3.进入代码根目录下，安装依赖
```javascript
npm install
```

4.安装插件依赖
```
npm install mammoth node-schedule --save
```

5.启动应用
```javascript
pm2 start server.js --name doracms2
```

6.停止服务
```javascript
pm2 stop doracms2
```

7.重启服务
```javascript
pm2 restart doracms2
```

