---
title: '简介'
---

# 简介

这里介绍了 `DoraCMS` 的使用场景，目录结构以及整体架构，让你从系统功能和代码层面上对 `DoraCMS` 有大致的了解

## 使用场景

### 普通建站者
您可以使用 `DoraCMS` 发布版进行网站快速搭建上线，选择您合适的皮肤切换风格，满足业务需求

### 开发者
您可以使用 `DoraCMS` 快速进行二次开发，上手容易，需要基本的前端知识就可以建站，`DoraCMS` 遵循 `MIT` 协议完全开源，所以代码可以免费商用。

## 功能模块
[点此查看大图](https://cdn.html-js.cn/cms/upload/ueditor/image/20191118/1574087349255394686.png)

![DoraCMS 功能结构图](https://cdn.html-js.cn/cms/upload/ueditor/image/20191118/1574087349255394686.png)

## 目录结构

### DoraCMS 项目目录结构如下

```
oas-system
├── README.md
├── app
│   ├── assets (静态资源源文件)
│   ├── bootstrap (模板渲染处理)
│   ├── controller (控制器)
│   ├── extend (程序扩展)
│   ├── middleware (中间件)
│   ├── model  (mongo数据模型)
│   ├── public (静态资源目录)
│   ├── router (路由))
│   ├── router.js
│   ├── schedule (定时任务)
│   ├── service (数据处理)
│   ├── utils (公共方法)
│   ├── validate (数据校验)
│   └── view (模板)
├── app.js
├── appveyor.yml
├── backstage (后台页面集合)
│   ├── adminGroup  (角色管理)
│   ├── adminResource  (资源管理)
│   ├── adminUser  (管理员管理)
│   ├── ads  (广告管理)
│   ├── announce  (系统公告管理)
│   ├── backUpData  (数据备份)
│   ├── build  (整体打包发布脚本)
│   ├── content (文档管理)
│   ├── contentCategory  (文档类别管理)
│   ├── contentMessage  (文档留言管理)
│   ├── contentTags   (文档标签管理)
│   ├── contentTemp  (模板编辑)
│   ├── dashboard  (后台首页))
│   ├── dist  (后台页面打包集合)
│   ├── helpCenter  (帮助中心)
│   ├── navbar  (右侧菜单)
│   ├── plugin  (插件管理)
│   ├── regUser  (会员管理)
│   ├── systemConfig  (系统配置)
│   ├── systemNotify   (系统通知)
│   ├── systemOptionLog   (操作日志)
│   ├── templateConfig   (模板配置)
│   ├── uploadFile   (文件上传)
│   └── versionManage  (APP版本管理)
├── build
│   │
│   └── restore.js (数据恢复)
├── config
│   ├── config.default.js  (系统默认配置)
│   ├── config.local.js  (开发环境配置)
│   ├── config.prod.js   (生产环境配置)
│   ├── locale  (国际化文件夹)
│   └── plugin.js  (插件配置)
├── data
│   │
│   └── doracms2.zip   (数据恢复)
│
├── index.js
├── lib
│   ├── framework.js
│   └── plugin
├── logs  (日志目录)
│
├── package.json
├── robots.txt  (seo robots)
├── run
│
└test
   ├── fixtures
   └── framework.test.js

```