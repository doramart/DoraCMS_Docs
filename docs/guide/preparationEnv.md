# 后台管理环境准备

#### 安装后台管理基础依赖
> 假设你已经搭建好开发环境，并执行了 npm run dev 进入开发模式

1.进入后台的 build 目录
> 路径 root -> backstage -> build

2.安装相关依赖
> 如果网络不好，可以尝试使用淘宝代理


```javascript
npm install
```

3.依然在build目录下，批量给所有后台模块安装依赖
```javascript
npm run installModules
```

4.把后台文件重新编译一次
```javascript
npm run buildDevModules
```